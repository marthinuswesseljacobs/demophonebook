﻿
namespace PhoneBook.Core.Interfaces
{
    public interface IConnection
    {
        string Name { get; set; }
        string ConnectionString { get; set; }
    }
}
