﻿namespace PhoneBook.Core.Interfaces
{
    public class Connection : IConnection
    {
        public string Name { get; set; }
        public  string ConnectionString { get; set; }
    }
}
