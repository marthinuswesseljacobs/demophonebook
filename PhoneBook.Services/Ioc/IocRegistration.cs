﻿using PhoneBook.Core.Ioc;
using PhoneBook.Services.PhoneBook;
using PhoneBook.Services.PhoneEntry;

namespace PhoneBook.Services.Ioc
{
    public static class IocRegistration
    {
        public static void Register()
        {
            IocContainer.RegisterType<PhoneBookService, IPhoneBookService>();
            IocContainer.RegisterType<PhoneEntryService, IPhoneEntryService>();
        }
    }
}
