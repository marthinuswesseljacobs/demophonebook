﻿using PhoneBook.Models;
using System.Collections.Generic;

namespace PhoneBook.Services.PhoneEntry
{
    public interface IPhoneEntryService
    {
        PhoneEntryModel AddPhoneEntry(string name, string number, int phoneBookId);
        List<PhoneEntryModel> Search(string searchParameter, int skip, int take);
    }
}
