﻿using System.Collections.Generic;
using PhoneBook.Models;
using PhoneBook.Repos.PhoneEntry;
using DemoPhoneBook.Filters;
using System.Linq;

namespace PhoneBook.Services.PhoneEntry
{
    public class PhoneEntryService : IPhoneEntryService
    {
        private IPhoneEntryRepo _phoneEntryRepo;

        public PhoneEntryService(IPhoneEntryRepo phoneEntryRepo)
        {
            _phoneEntryRepo = phoneEntryRepo;
        }

        public PhoneEntryModel AddPhoneEntry(string name, string number, int phoneBookId)
        {
            return _phoneEntryRepo.Add(new PhoneEntryModel {
                Name = name,
                Number = number,
                PhoneBookId = phoneBookId
            });
        }

        public List<PhoneEntryModel> Search(string query, int skip, int take = 10)
        {
            var collection = _phoneEntryRepo.GetCollection(new PhoneEntryFilter {
                Name = query,
                Number = query
            });

            //only doing pagination here for demo purposes.
            //usually i let the data query take care of pagination.
            if(skip > 0 && collection.Count > skip)
                collection = collection.Skip(skip).ToList();

            collection = collection.Take(take).ToList();

            return collection;
        }
    }
}
