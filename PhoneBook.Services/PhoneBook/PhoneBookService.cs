﻿using PhoneBook.Models;
using PhoneBook.Repos.PhoneBook;

namespace PhoneBook.Services.PhoneBook
{
    public class PhoneBookService : IPhoneBookService
    {
        private IPhoneBookRepo _phoneBookRepo;

        public PhoneBookService(IPhoneBookRepo phoneBookRepo)
        {
            _phoneBookRepo = phoneBookRepo;
        }

        public PhoneBookModel AddPhoneBook(string name)
        {
            return _phoneBookRepo.Add(new PhoneBookModel {
                Name = name
            });
        }
    }
}
