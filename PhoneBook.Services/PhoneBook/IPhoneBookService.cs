﻿using PhoneBook.Models;

namespace PhoneBook.Services.PhoneBook
{
    public interface IPhoneBookService
    {
        PhoneBookModel AddPhoneBook(string name);
    }
}
