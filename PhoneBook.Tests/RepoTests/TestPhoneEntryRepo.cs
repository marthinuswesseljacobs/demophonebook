using DemoPhoneBook.Filters;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PhoneBook.Core.Implementations;
using PhoneBook.Core.Interfaces;
using PhoneBook.Core.Ioc;
using PhoneBook.Models;
using PhoneBook.Repos.PhoneEntry;

namespace PhoneBook.RepoTests.TestPhoneEntryRepo
{
    [TestClass]
    public class TestPhoneEntryRepo
    {
        public TestPhoneEntryRepo()
        {
            IocContainer.Reset();
            Repos.Ioc.IocRegistration.Register();
            IocContainer.RegisterType<TestRepo, IRepoService>();
            IocContainer.Build();
        }

        [TestMethod]
        public void PhoneEntryRepo_Add()
        {
            var _priceRepo = IocContainer.Resolve<IPhoneEntryRepo>();

            var added = _priceRepo.Add(new PhoneEntryModel
            {
                PhoneEntryId = 1,
                Name = "Demo Name",
                Number = "12345"
            });

            Assert.IsTrue(added.PhoneEntryId >= 0);
            Assert.IsTrue(added.PhoneEntryId == 1);
            Assert.AreEqual("Demo Name", added.Name);
            Assert.AreEqual("12345", added.Number);

            _priceRepo.Delete(added.PhoneEntryId);
        }

        [TestMethod]
        public void PhoneEntryRepo_Delete()
        {
            var _priceRepo = IocContainer.Resolve<IPhoneEntryRepo>();

            var added = _priceRepo.Add(new PhoneEntryModel
            {
                PhoneEntryId = 1,
                Name = "Demo Name",
                Number = "12345"
            });

            var deleted = _priceRepo.Delete(added.PhoneEntryId);

            Assert.AreEqual(added.PhoneEntryId, deleted.PhoneEntryId);
            Assert.AreEqual(added.Name, deleted.Name);
            Assert.AreEqual(added.Number, deleted.Number);
        }

        [TestMethod]
        public void PhoneEntryRepo_Edit()
        {
            var _priceRepo = IocContainer.Resolve<IPhoneEntryRepo>();

            var added = _priceRepo.Add(new PhoneEntryModel
            {
                PhoneEntryId = 1,
                Name = "Demo Name",
                Number = "12345"
            });

            added.PhoneEntryId = 2;
            added.Name = "Updated Name";
            added.Number = "56785678";

            var edited = _priceRepo.Edit(added);

            Assert.AreEqual(added.PhoneEntryId, edited.PhoneEntryId);
            Assert.AreEqual(added.Name, edited.Name);
            Assert.AreEqual(added.Number, edited.Number);

            _priceRepo.Delete(added.PhoneEntryId);
        }

        [TestMethod]
        public void PhoneEntryRepo_GetModel()
        {
            var _priceRepo = IocContainer.Resolve<IPhoneEntryRepo>();

            var added = _priceRepo.Add(new PhoneEntryModel
            {
                PhoneEntryId = 1,
                Name = "Demo Name",
                Number = "12345"
            });

            var getModel = _priceRepo.GetModel(added.PhoneEntryId);

            Assert.AreEqual(added.PhoneEntryId, getModel.PhoneEntryId);
            Assert.AreEqual(added.Name, getModel.Name);
            Assert.AreEqual(added.Number, getModel.Number);

            _priceRepo.Delete(added.PhoneEntryId);
        }

        [TestMethod]
        public void PhoneEntryRepo_GetCollection()
        {
            var _priceRepo = IocContainer.Resolve<IPhoneEntryRepo>();

            _priceRepo.Add(new PhoneEntryModel
            {
                PhoneEntryId = 1,
                Name = "Demo Name",
                Number = "12345"
            });
            _priceRepo.Add(new PhoneEntryModel
            {
                PhoneEntryId = 2,
                Name = "Demo Name",
                Number = "12345"
            });
            _priceRepo.Add(new PhoneEntryModel
            {
                PhoneEntryId = 3,
                Name = "Demo Name",
                Number = "12345"
            });

            var collection = _priceRepo.GetCollection();

            Assert.AreEqual(3, collection.Count);

            foreach (var item in collection)
            {
                _priceRepo.Delete(item.PhoneEntryId);
            }
        }

        
        [TestMethod]
        public void PhoneEntryRepo_GetCollectionByFilter()
        {
            var _priceRepo = IocContainer.Resolve<IPhoneEntryRepo>();
            
            _priceRepo.Add(new PhoneEntryModel
            {
                PhoneEntryId = 1,
                Name = "Demo Name 1",
                Number = "111"
            });
            _priceRepo.Add(new PhoneEntryModel
            {
                PhoneEntryId = 2,
                Name = "Demo Name 2",
                Number = "222"
            });
            _priceRepo.Add(new PhoneEntryModel
            {
                PhoneEntryId = 3,
                Name = "Demo Name 3",
                Number = "333"
            });

            var collection = _priceRepo.GetCollection(new PhoneEntryFilter
            {
                Name = "Demo Name 2",
                Number = "333"
            });

            Assert.AreEqual(2, collection.Count);

            foreach (var item in collection)
            {
                _priceRepo.Delete(item.PhoneEntryId);
            }
        }
    }
}
