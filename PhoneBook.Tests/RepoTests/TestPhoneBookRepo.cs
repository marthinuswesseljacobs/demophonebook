using Microsoft.VisualStudio.TestTools.UnitTesting;
using PhoneBook.Core.Implementations;
using PhoneBook.Core.Interfaces;
using PhoneBook.Core.Ioc;
using PhoneBook.Models;
using PhoneBook.Repos.PhoneBook;

namespace PhoneBook.RepoTests.TestPhoneBookRepo
{
    [TestClass]
    public class TestPhoneBookRepo
    {
        public TestPhoneBookRepo()
        {
            IocContainer.Reset();
            Repos.Ioc.IocRegistration.Register();
            IocContainer.RegisterType<TestRepo, IRepoService>();
            IocContainer.Build();
        }

        [TestMethod]
        public void PhoneBookRepo_Add()
        {
            var _priceRepo = IocContainer.Resolve<IPhoneBookRepo>();

            var added = _priceRepo.Add(new PhoneBookModel
            {
                PhoneBookId = 1,
                Name = "Demo Name"
            });

            Assert.IsTrue(added.PhoneBookId >= 0);
            Assert.IsTrue(added.PhoneBookId == 1);
            Assert.AreEqual("Demo Name", added.Name);

            _priceRepo.Delete(added.PhoneBookId);
        }

        [TestMethod]
        public void PhoneBookRepo_Delete()
        {
            var _priceRepo = IocContainer.Resolve<IPhoneBookRepo>();

            var added = _priceRepo.Add(new PhoneBookModel
            {
                PhoneBookId = 1,
                Name = "Demo Name"
            });

            var deleted = _priceRepo.Delete(added.PhoneBookId);

            Assert.AreEqual(added.PhoneBookId, deleted.PhoneBookId);
            Assert.AreEqual(added.Name, deleted.Name);
            Assert.AreEqual(added.Entries, deleted.Entries);
        }

        [TestMethod]
        public void PhoneBookRepo_Edit()
        {
            var _priceRepo = IocContainer.Resolve<IPhoneBookRepo>();

            var added = _priceRepo.Add(new PhoneBookModel
            {
                PhoneBookId = 1,
                Name = "Demo Name"
            });

            added.PhoneBookId = 2;
            added.Name = "Updated BarCode";

            var edited = _priceRepo.Edit(added);

            Assert.AreEqual(added.PhoneBookId, edited.PhoneBookId);
            Assert.AreEqual(added.Name, edited.Name);
            Assert.AreEqual(added.Entries, edited.Entries);

            _priceRepo.Delete(added.PhoneBookId);
        }

        [TestMethod]
        public void PhoneBookRepo_GetModel()
        {
            var _priceRepo = IocContainer.Resolve<IPhoneBookRepo>();

            var added = _priceRepo.Add(new PhoneBookModel
            {
                PhoneBookId = 1,
                Name = "Demo Name"
            });

            var getModel = _priceRepo.GetModel(added.PhoneBookId);

            Assert.AreEqual(added.PhoneBookId, getModel.PhoneBookId);
            Assert.AreEqual(added.Name, getModel.Name);
            Assert.AreEqual(added.Entries, getModel.Entries);

            _priceRepo.Delete(added.PhoneBookId);
        }

        [TestMethod]
        public void PhoneBookRepo_GetCollection()
        {
            var _priceRepo = IocContainer.Resolve<IPhoneBookRepo>();

            _priceRepo.Add(new PhoneBookModel
            {
                PhoneBookId = 1,
                Name = "Demo Name"
            });
            _priceRepo.Add(new PhoneBookModel
            {
                PhoneBookId = 2,
                Name = "Demo Name"
            });
            _priceRepo.Add(new PhoneBookModel
            {
                PhoneBookId = 3,
                Name = "Demo Name"
            });

            var collection = _priceRepo.GetCollection();

            Assert.AreEqual(3, collection.Count);

            foreach (var item in collection)
            {
                _priceRepo.Delete(item.PhoneBookId);
            }
        }
    }
}
