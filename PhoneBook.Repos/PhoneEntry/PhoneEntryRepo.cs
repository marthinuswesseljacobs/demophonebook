﻿using System.Collections.Generic;
using PhoneBook.Models;
using PhoneBook.Core.Interfaces;
using DemoPhoneBook.Filters;

namespace PhoneBook.Repos.PhoneEntry
{
    public class PhoneEntryRepo : IPhoneEntryRepo
    {
        private IRepoService _repo;

        public PhoneEntryRepo(IRepoService repo)
        {
            _repo = repo;
        }

        public PhoneEntryModel Add(PhoneEntryModel model)
        {
            return _repo.Add<PhoneEntryModel>("usp_PhoneEntryAdd", model);
        }


        public PhoneEntryModel Delete(int id)
        {
            return _repo.Delete<PhoneEntryModel>("usp_PhoneEntryDelete", new { PhoneEntryId = id });
        }

        public PhoneEntryModel Edit(PhoneEntryModel model)
        {
            return _repo.Edit<PhoneEntryModel>("usp_PhoneEntryEdit", model);
        }

        public PhoneEntryModel GetModel(int id)
        {
            return _repo.GetModel<PhoneEntryModel>("usp_PhoneEntryGetModel", new { PhoneEntryId = id });
        }

        public List<PhoneEntryModel> GetCollection()
        {
            return _repo.GetCollection<PhoneEntryModel>("usp_PhoneEntryGetCollection");
        }

        public List<PhoneEntryModel> GetCollection(PhoneEntryFilter filter)
        {
            return _repo.GetCollection<PhoneEntryModel>("usp_PhoneEntryGetCollectionByFilter", filter);
        }
    }
}
