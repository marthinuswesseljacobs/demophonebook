﻿using DemoPhoneBook.Filters;
using PhoneBook.Core.Interfaces;
using PhoneBook.Models;
using System.Collections.Generic;

namespace PhoneBook.Repos.PhoneEntry
{
    public interface IPhoneEntryRepo : IRepoContract<PhoneEntryModel>
    {
        new PhoneEntryModel Add(PhoneEntryModel model);
        new PhoneEntryModel Delete(int id);
        new PhoneEntryModel Edit(PhoneEntryModel model);
        new PhoneEntryModel GetModel(int id);
        new List<PhoneEntryModel> GetCollection();
        List<PhoneEntryModel> GetCollection(PhoneEntryFilter filter);
    }
}
