﻿using System.Collections.Generic;
using PhoneBook.Models;
using PhoneBook.Core.Interfaces;

namespace PhoneBook.Repos.PhoneBook
{
    public class PhoneBookRepo : IPhoneBookRepo
    {
        private IRepoService _repo;

        public PhoneBookRepo(IRepoService repo)
        {
            _repo = repo;
        }

        public PhoneBookModel Add(PhoneBookModel model)
        {
            return _repo.Add<PhoneBookModel>("usp_PhoneBookAdd", model);
        }


        public PhoneBookModel Delete(int id)
        {
            return _repo.Delete<PhoneBookModel>("usp_PhoneBookDelete", new { PhoneBookId = id });
        }

        public PhoneBookModel Edit(PhoneBookModel model)
        {
            return _repo.Edit<PhoneBookModel>("usp_PhoneBookEdit", model);
        }

        public PhoneBookModel GetModel(int id)
        {
            return _repo.GetModel<PhoneBookModel>("usp_PhoneBookGetModel", new { PhoneBookId = id });
        }

        public List<PhoneBookModel> GetCollection()
        {
            return _repo.GetCollection<PhoneBookModel>("usp_PhoneBookGetCollection");
        }
    }
}
