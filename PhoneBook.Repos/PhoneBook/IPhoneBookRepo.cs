﻿using PhoneBook.Core.Interfaces;
using PhoneBook.Models;
using System.Collections.Generic;

namespace PhoneBook.Repos.PhoneBook
{
    public interface IPhoneBookRepo : IRepoContract<PhoneBookModel>
    {
        new PhoneBookModel Add(PhoneBookModel model);
        new PhoneBookModel Delete(int id);
        new PhoneBookModel Edit(PhoneBookModel model);
        new PhoneBookModel GetModel(int id);
        new List<PhoneBookModel> GetCollection();
    }
}
