﻿using PhoneBook.Core.Implementations;
using PhoneBook.Core.Interfaces;
using PhoneBook.Core.Ioc;
using PhoneBook.Repos.PhoneBook;
using PhoneBook.Repos.PhoneEntry;

namespace PhoneBook.Repos.Ioc
{
    public static class IocRegistration
    {
        public static void Register()
        {
            IocContainer.RegisterType<Connection, IConnection>();
            IocContainer.RegisterType<SqlRepo, IRepoService>();

            IocContainer.RegisterType<PhoneBookRepo, IPhoneBookRepo>();
            IocContainer.RegisterType<PhoneEntryRepo, IPhoneEntryRepo>();

        }
    }
}
