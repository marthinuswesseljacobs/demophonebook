1. Open the solution.
2. Build the solution.
3. Run the PhoneBook.Api project.
4. Download the Postman app here https://www.getpostman.com/apps.
4. Import the Postman collection json file located under the 'Postman Collections' folder into the Postman App.
5. Use the Postman collections to interact with the API.
	-Phone Entries - Search
	-Phone Entries - Add
	-Phone Entries - Search 'demo'