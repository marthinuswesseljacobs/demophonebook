﻿namespace DemoPhoneBook.Filters
{
    public class PhoneEntryFilter
    {
        public string Name { get; set; }
        public string Number { get; set; }
    }
}
