﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Autofac;
using PhoneBook.Core.Implementations;
using PhoneBook.Core.Interfaces;
using Autofac.Extensions.DependencyInjection;
using PhoneBook.Api.Controllers;
using PhoneBook.Models;

namespace PhoneBook.Api
{
    public class Startup
    {
        public IContainer ApplicationContainer { get; private set; }
        public IConfigurationRoot Configuration { get; private set; }

        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }
        
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            // Add services to the collection.
            services.AddMvc().AddControllersAsServices();
            services.AddCors(o => o.AddPolicy("CorsPolicy", builder =>
            {
                builder.AllowAnyOrigin()
                       .AllowAnyMethod()
                       .AllowAnyHeader();
            }));

            //controlelr registrations
            Core.Ioc.IocContainer.ContainerBuilder.RegisterType<PhoneEntryController>().PropertiesAutowired();

            //service population
            Core.Ioc.IocContainer.ContainerBuilder.Populate(services);

            //component registrations
            Repos.Ioc.IocRegistration.Register();
            Services.Ioc.IocRegistration.Register();
            Core.Ioc.IocContainer.RegisterType<MoqRepo, IRepoService>();

            //build ioc container
            Core.Ioc.IocContainer.Build();

            ApplicationContainer = Core.Ioc.IocContainer.Container;

            //load moq data into memory for demo purposes.
            MoqRepo.LoadJsonCollection<PhoneBookModel>("MoqData/PhoneBooks.json");
            MoqRepo.LoadJsonCollection<PhoneEntryModel>("MoqData/PhoneEntries.json");

            return new AutofacServiceProvider(ApplicationContainer);
        }
        
        public void Configure(
          IApplicationBuilder app,
          ILoggerFactory loggerFactory,
          IApplicationLifetime appLifetime)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            app.UseMvc();
            appLifetime.ApplicationStopped.Register(() => ApplicationContainer.Dispose());
        }
    }
}
