using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using PhoneBook.Api.CustomAttributes;
using PhoneBook.Services.PhoneBook;

namespace PhoneBook.Api.Controllers
{
    [Produces("application/json")]
    [Route("api/v1/phonebooks")]
    [GeneralExceptionFilter]
    [EnableCors("CorsPolicy")]
    public class PhoneBookController : Controller
    {
        private IPhoneBookService _phoneBookService;

        public PhoneBookController(IPhoneBookService phoneBookService)
        {
            _phoneBookService = phoneBookService;
        }

        [HttpPost("")]
        public IActionResult Post(string name) {
            var result = _phoneBookService.AddPhoneBook(name);
            return Ok(result);
        }
    }
}