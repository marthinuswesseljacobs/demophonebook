using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using PhoneBook.Api.CustomAttributes;
using PhoneBook.Services.PhoneEntry;

namespace PhoneBook.Api.Controllers
{
    [Produces("application/json")]
    [Route("api/v1/phoneentries")]
    [GeneralExceptionFilter]
    [EnableCors("CorsPolicy")]
    public class PhoneEntryController : Controller
    {
        private IPhoneEntryService _phoneEntryService;

        public PhoneEntryController(IPhoneEntryService phoneEntryService)
        {
            _phoneEntryService = phoneEntryService;
        }

        [HttpPost("")]
        public IActionResult Post(string name, string number, int phoneBookId)
        {
            
            var result = _phoneEntryService.AddPhoneEntry(name, number, phoneBookId);
            return Ok(result);
        }
        

        [HttpGet("search")]
        public IActionResult Search(string query = "", int skip = 0, int take = 10)
        {
            var result = _phoneEntryService.Search(query, skip, take);
            return Ok(result);
        }

    }
}