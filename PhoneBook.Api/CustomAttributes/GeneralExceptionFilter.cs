﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Net.Http.Headers;
using System.Net;
using System.Text;

namespace PhoneBook.Api.CustomAttributes
{
    public class GeneralExceptionFilter : ExceptionFilterAttribute
    {
        //implement a very basic exception handler
        public override void OnException(ExceptionContext context)
        {
            context.ExceptionHandled = true;
            context.HttpContext.Response.Clear();
            context.HttpContext.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
            context.HttpContext.Response.ContentType = new MediaTypeHeaderValue("text/html").ToString();
            context.HttpContext.Response.WriteAsync(context.Exception.Message.ToString(), Encoding.UTF8);
        }
    }
}
