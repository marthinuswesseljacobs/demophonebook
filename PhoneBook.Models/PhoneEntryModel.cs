﻿
namespace PhoneBook.Models
{
    public class PhoneEntryModel
    {
        public int PhoneEntryId { get; set; }
        public int PhoneBookId { get; set; }
        public string Name { get; set; }
        public string Number { get; set; }
    }
}
