﻿using System.Collections.Generic;

namespace PhoneBook.Models
{
    public class PhoneBookModel
    {
        public int PhoneBookId { get; set; }
        public string Name { get; set; }
        public List<PhoneEntryModel> Entries { get; set; }
    }
}
